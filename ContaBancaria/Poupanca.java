

public class Poupanca extends Conta {
    
    
    public Poupanca(int numero) {
        super(numero); 
    }
    
    public void rendimento(double taxaJuros) {
        double rendimento = getSaldo() * taxaJuros;
        depositar(rendimento);
    }

}
