import java.util.ArrayList;


public class Cliente
{
    private String nome;
    private ArrayList<Conta> contas;
    
    public Cliente() {
        this.contas = new ArrayList<>();   
    }
    
    public double saldoTotal() {
        double total = 0;
        
        for(Conta c : contas) {
            total += c.getSaldo();   
        }
        
        return total;
    }
    
    public void setNome (String nome) {
        this.nome = nome;   
    }
    
    public String getNome() {
        return this.nome;   
    }
    
    public void addConta (Conta contas) {
        this.contas.add(contas);   
    }
    
    public ArrayList<Conta> getContas() {
        return this.contas;    
    }
}
