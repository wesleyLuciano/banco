import java.util.Random;

public class Investimento
{
    private String descricao;
    private double valorInvestido;
    
    public Investimento() {
    }
    
    public Investimento(String descricao, double valorInvestido) {
        this.descricao = descricao;
        this.valorInvestido = valorInvestido;
    }
    
    public double valorAtualInvestimento() {
        Random r = new Random();
        
        return this.valorInvestido * (r.nextDouble() + 0.4);   
    }
    
    public void setDescricao (String descricao) {
        this.descricao = descricao;   
    }
    
    public String getDescricao() {
        return this.descricao;   
    }
    
    public void setValorInvestido(double valorInvestido) {
        this.valorInvestido = valorInvestido;   
    }
    
    public double getValorInvestido() {
        return this.valorInvestido;   
    }
    
}
