

public class ContaCorrente extends Conta
{
    private double limiteCredito;
    
    public ContaCorrente(int numero) {
        super(numero);   
    }
    
    public ContaCorrente (int numero, double limiteCredito) {
        super(numero);
        this.limiteCredito = limiteCredito;
    }
    
    @Override
    public void sacar(double valor) {
        double saldo = getSaldo();
        
        if((saldo + limiteCredito) >= valor) {
            saldo -= valor;   
        }
    }
    
    public void setLimiteCredito(double limiteCredito) {
        this.limiteCredito = limiteCredito;   
    }
    
    public double getLimiteCredito() {
        return this.limiteCredito;   
    }
}
