
public class Conta
{
    private int numero;
    private double saldo;
    
    public Conta(int numero) {
        this.numero = numero;
        this.saldo = 0;
    }
    
    public void depositar(double valor) {
        this.saldo += valor;   
    }
    
    public void sacar(double valor) {
        if(this.saldo >= valor){
            this.saldo -= valor;
        }
    }
    
    public void setNumero (int numero) {
        this.numero = numero;   
    }
    
    public double getNumero() {
        return this.numero;   
    }
    
    public double getSaldo() {
        return this.saldo;   
    }
}
